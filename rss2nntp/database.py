import sqlite3


class Database:
    def __init__(self, path):
        self._conn = sqlite3.connect(path)
        self.initialise_dabase()

    def initialise_dabase(self):
        self._conn.execute('CREATE TABLE IF NOT EXISTS '
                           'posts (id VARCHAR(32) PRIMARY KEY);')
        self._conn.execute('CREATE UNIQUE INDEX IF NOT EXISTS '
                           'post_index ON posts(id);')

    def is_read(self, id):
        c = self._conn.cursor()
        c.execute('SELECT COUNT(id) FROM posts WHERE id = ?', (id,))
        count = c.fetchone()[0]
        return count == 1

    def insert(self, id):
        self._conn.execute('INSERT INTO posts VALUES (?);', (id,))
        self._conn.commit()
