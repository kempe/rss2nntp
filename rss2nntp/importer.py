import feedparser
import nntplib
import hashlib

from .database import Database


class Importer:
    def __init__(self, url, parser, database, server, user, password):
        self._read = dict()
        self._url = url
        self._parser = parser
        self._db = Database(database)
        self._nntp = nntplib.NNTP_SSL(server, user=user, password=password)

    def poll(self):
        d = feedparser.parse(self._url)
        for entry in d['entries']:
            h = hashlib.md5()
            try:
                h.update((entry.title + entry.published).encode('utf-8'))

                if entry.title == '':
                    continue
            except AttributeError:
                continue

            key = h.digest().hex()

            if not self._db.is_read(key):
                post = self._parser.parse(entry)
                self._nntp.post(post)
                self._db.insert(key)
