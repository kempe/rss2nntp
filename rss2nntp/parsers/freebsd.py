import textwrap
import urllib.request


post_template = '''From: {sender}
Newsgroups: {group}
Subject: {subject}
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

{body}

Publicerad: {date}
'''


class FreeBSD:
    def __init__(self, sender, group):
        self._sender = sender
        self._group = group

    def _download_id(self, link):
        entry = None
        for _ in range(5):
            try:
                resp = urllib.request.urlopen(link)
                entry = resp.read().decode('utf-8')
                break
            except Exception:
                continue
        return entry

    def parse(self, entry):
        try:
            body = entry.summary
        except AttributeError:
            if entry.id.endswith(".asc"):
                body = self._download_id(entry.id)

            if body is None:
                body = entry.title

        if len(body.split('\n')[0]) > 80:
            body = textwrap.fill(body, width=80)

        return post_template.format(sender=self._sender,
                                    subject=entry.title,
                                    body=body,
                                    group=self._group,
                                    date=entry.published).encode('utf-8')
