import html2text


post_template = '''From: {sender}
Newsgroups: {group}
Subject: {subject}
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

{body}

Publicerad: {date}
'''


class Voidlinux:
    def __init__(self, group, sender):
        self._sender = sender
        self._group = group

    def parse(self, entry):
        h = html2text.HTML2Text()
        h.ignore_emphasis = True
        h.ignore_links = False
        h.ignore_images = True

        body = h.handle(entry.summary)

        return post_template.format(sender=self._sender,
                                    subject=entry.title,
                                    body=body,
                                    group=self._group,
                                    date=entry.published).encode('utf-8')
